import {combineReducers} from 'redux';
import building from './building';

export default combineReducers({
    building,
});

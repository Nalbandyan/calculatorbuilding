import {
    SET_BUILDING_TYPE,
    SET_HEIGHT,
    CLEAR_DATA,
    SET_MATERIAL,
    SET_SIZE,
    SET_RESULT
} from '../actions/building';

const initialState = {
    buildingType: '1',
    material: '1',
    height: '',
    sizex: '',
    sizey: '',
    result: {}
};

export default function reducer(state: object = initialState, action: any) {
    switch (action.type) {
        case SET_BUILDING_TYPE: {
            return {
                ...state,
                buildingType: action.payload.type
            };
        }
        case SET_HEIGHT: {
            return {
                ...state,
                height: action.payload.height
            };
        }

        case SET_MATERIAL: {
            return {
                ...state,
                material: action?.payload?.material
            };
        }

        case SET_SIZE: {
            return {
                ...state,
                sizex: action?.payload?.size?.sizex,
                sizey: action?.payload?.size?.sizey
            };
        }

        case SET_RESULT: {
            return {
                ...state,
                result: action?.payload?.result
            };
        }
        case CLEAR_DATA: {
            return {
                buildingType: '1',
                material: '1',
                height: '',
                sizex: '',
                sizey: '',
            };
        }
        default: {
            return state;
        }
    }
}

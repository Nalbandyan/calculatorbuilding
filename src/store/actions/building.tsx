export const SET_BUILDING_TYPE = 'SET_BUILDING_TYPE';

export function setBuildingType(type: string) {
    return {
        type: SET_BUILDING_TYPE,
        payload: {type},
    };
}

export const SET_HEIGHT = 'SET_HEIGHT';

export function setHeight(height: string) {
    return {
        type: SET_HEIGHT,
        payload: {height},
    };
}

export const SET_MATERIAL = 'SET_MATERIAL';

export function setMaterial(material: string) {
    return {
        type: SET_MATERIAL,
        payload: {material}
    };
}

export const SET_SIZE = 'SET_SIZE';

export function setSize(size: any) {
    return {
        type: SET_SIZE,
        payload: {size}
    };
}

export const CLEAR_DATA = 'CLEAR_DATA';

export function clearData() {
    return {
        type: CLEAR_DATA,
    };
}

export const CALCULATE_DATA_REQUEST = 'CALCULATE_DATA_REQUEST';
export const CALCULATE_DATA_SUCCESS = 'CALCULATE_DATA_SUCCESS';
export const CALCULATE_DATA_FAIL = 'CALCULATE_DATA_FAIL';

export function calculateDataRequest(data: object, cb: any) {
    return {
        type: CALCULATE_DATA_REQUEST,
        payload: {data, cb}
    };
}

export const SET_RESULT = 'SET_RESULT';

export function setResult(result: any) {
    return {
        type: SET_RESULT,
        payload: {result}
    };
}
import {takeLatest, call, put} from 'redux-saga/effects';
import {
    CALCULATE_DATA_REQUEST,
    CALCULATE_DATA_SUCCESS,
    CALCULATE_DATA_FAIL
} from '../actions/building';
import Api from '../../Api';

function* calculateData(action:any) {
    try {
        const {data} = yield call(Api.calculateData, action.payload.data);
        yield put({
            type: CALCULATE_DATA_SUCCESS,
            payload: {data},
        });
        if (action.payload.cb) {
            action.payload.cb(null, data);
        }
    } catch (e) {
        if (action.payload.cb) {
            action.payload.cb(e);
        }
        yield put({
            type: CALCULATE_DATA_FAIL,
            message: e.message,
        });
    }
}

export default function* watcher() {
    yield takeLatest(CALCULATE_DATA_REQUEST, calculateData);
}


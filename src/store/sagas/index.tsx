import { all, fork } from 'redux-saga/effects';

import building from './building';

export default function* watchers() {
    yield all([
        building
    ].map(fork));
}

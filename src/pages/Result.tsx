import React, {FunctionComponent} from 'react';
import {useHistory} from 'react-router-dom';
import {clearData, setBuildingType} from "../store/actions/building";
import {connect} from "react-redux";

const Result: FunctionComponent = (props: any) => {
    let history = useHistory()
    let {result, clearData} = props;

    return (
        <div className="wrapper result">
          <p className="title">Калькулятор цены конструкций</p>
            <p className="step">Резултат рассчета</p>
            <div className="body__container">
                {result.result === "ok" ?
                    <div  className="result__container">
                        <p className="container__name">Успешно</p>
                        <p className="success__text">{result.message || ''}</p>
                    </div>
                    :
                    <div className="result__container">
                        <p className="container__name">Ошибка</p>
                        <p className="error__text">{result.message || ''}</p>
                    </div>
                }
            </div>
            <div className="button__container">
                <input
                    onClick={() => {
                        clearData()
                        history.push('/')
                    }}
                    className="button"
                    type="button"
                    value="Новый расчет"
                />
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    result: state.building.result
});

const mapDispatchToProps = {
    setBuildingType,
    clearData
};

const Container = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Result);

export default Container;

import React, {FunctionComponent, useState} from 'react';
import {calculateDataRequest, clearData, setResult, setSize} from "../store/actions/building";
import {connect} from "react-redux";
import {useHistory} from "react-router-dom";

const FourthStep: FunctionComponent = (props: any) => {
    let [response, setResponse] = useState<any>({});
    let {setResult,buildingType,height, material, clearData, sizex, sizey, setSize, calculateDataRequest} = props;
    let history = useHistory()
    const validateAndRedirect = () => {
        calculateDataRequest({
            building:buildingType,
            height,
            material,
            sizex,
            sizey
        }, (val:null, data:any) => {
            setResponse(data)
            setResult(data)
            history.push("/result")
        })
    }
    return (
        <div className="wrapper step__four">
          <p className="title">Калькулятор цены конструкций</p>
          <p className="step">Шаг 4</p>
            <div className="body__container">
                <p className="container__name">Длина стен (в метрах):</p>
                <div className="operating__container">
                 <input
                     className="input"
                     type="text"
                     onChange={(ev: any) => setSize({
                         sizex: ev.target.value,
                         sizey,
                     })}
                     value={sizex}
                 />
                 <p className="x">X</p>
                 <input
                     className="input"
                     type="text"
                     onChange={(ev: any) => setSize({
                         sizey: ev.target.value,
                         sizex
                     })}
                     value={sizey}
                 />
                 <p>{response.message || null}</p>
                </div>

            </div>
            <div className="button__container">
                <input
                    onClick={() => {
                        clearData()
                        history.push('/')
                    }}
                    type="button"
                    value="Отмена"
                    className="button"
                />
                <input
                    type="submit"
                    value="Рассчитать"
                    className="button"
                    onClick={validateAndRedirect}
                />
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    buildingType: state.building.buildingType,
    material: state.building.material || '1',
    height: state.building.height,
    sizex: state.building.sizex,
    sizey: state.building.sizey
});

const mapDispatchToProps = {
    setSize,
    setResult,
    clearData,
    calculateDataRequest
};

const Container = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FourthStep);

export default Container;
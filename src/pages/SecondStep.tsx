import React, {FunctionComponent, useState} from 'react';
import {clearData, setHeight} from "../store/actions/building";
import {connect} from "react-redux";
import {useHistory} from "react-router-dom";

const SecondStep: FunctionComponent = (props: any) => {
    let [error, setError] = useState<string | null>(null);
    let {setHeight, height, clearData} = props;
    let history = useHistory()
    const validateAndRedirect = () => {
        if (Number(height)) {
            setError(null)
            history.push('/third-step')
        } else {
            setError('Пожалуйста, введите только цифры!')
        }
    }
    return (
        <div className="wrapper step__two">
          <p className="title">Калькулятор цены конструкций</p>
          <p className="step">Шаг 2</p>
            <div className="body__container">
                <p className="container__name">Количество этажей (число):</p>
                <div>
                 <input
                     className="input"
                     type="text"
                     onChange={(ev: any) => setHeight(ev.target.value)}
                     value={height}
                 />
                 <p className="message">{error || null}</p>
                </div>
            </div>
            <div className="button__container">
                <input
                    onClick={() => {
                        clearData()
                        history.push('/')
                    }}
                    type="button"
                    className="button"
                    value="Отмена"
                />
                <input
                    type="submit"
                    value="Далее"
                    className="button"
                    onClick={validateAndRedirect}
                />
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    height: state.building.height
});

const mapDispatchToProps = {
    setHeight,
    clearData
};

const Container = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SecondStep);

export default Container;
import React, {FunctionComponent} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {clearData, setBuildingType} from "../store/actions/building";
import {connect} from "react-redux";

const FirstStep: FunctionComponent = (props: any) => {
    let history = useHistory()
    let {buildingType, setBuildingType, clearData} = props;
    return (
        <div className="wrapper">
          <p className="title">Калькулятор цены конструкций</p>
          <p className="step">Шаг 1</p>
            <div className="body__container">
              <p className="container__name">Что будем строить?</p>
              <div className="operating__container">
               <div>
                <input
                    type="radio"
                    checked={buildingType === '1'}
                    onChange={() => setBuildingType('1')}
                    value="1"
                    name="home"
                />
                 <Link
                     className="building__type__name"
                     onClick={() => setBuildingType('1')}
                     to="/second-step"
                 >
                     Жилой дом
                 </Link>
                    </div>
                    <div>
                     <input
                         type="radio"
                         checked={buildingType === '2'}
                         onChange={() => setBuildingType('2')}
                         value="2"
                         name="garage"
                     />
                     <Link
                         className="building__type__name"
                         onClick={() => setBuildingType('2')}
                         to="/third-step"
                     >
                         Гараж
                     </Link>
                    </div>
                </div>
            </div>
            <div className="button__container">
                <input
                    onClick={() => clearData()}
                    type="button"
                    value="Отмена"
                    className="button"
                />
                <input
                    type="submit"
                    value="Далее"
                    onClick={() => {
                        if (buildingType === '1') {
                            history.push('/second-step')
                        } else {
                            history.push('/third-step')
                        }
                    }}
                    className="button"
                />
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    buildingType: state.building.buildingType
});

const mapDispatchToProps = {
    setBuildingType,
    clearData
};

const Container = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FirstStep);

export default Container;

import React, {FunctionComponent} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {clearData, setMaterial} from "../store/actions/building";
import {connect} from "react-redux";

const ThirdStep: FunctionComponent = (props: any) => {
    let history = useHistory()
    let {material, setMaterial, clearData, buildingType} = props;
    return (
        <div className="wrapper">
          <p className="title">Калькулятор цены конструкций</p>
          <p className="step">Шаг 3</p>
            {buildingType === '1' ?
                <div  className="body__container">
                <p className="container__name">Материал стен:</p>
                    <div>
                        <div>
                 <input
                     type="radio"
                     checked={material === '1'}
                     onChange={() => setMaterial('1')}
                     value="1"
                 />
                 <Link className="building__type__name" onClick={() => setMaterial('1')}
                       to="/fourth-step">Кирпич</Link>
                            </div>
                        <div>
                 <input
                     type="radio"
                     checked={material === '2'}
                     onChange={() => setMaterial('2')}
                     value="2"
                 />
                 <Link className="building__type__name" onClick={() => setMaterial('2')}
                       to="/fourth-step">Шлакоблок</Link>
                            </div>
                        <div>
                <input
                    type="radio"
                    checked={material === '3'}
                    onChange={() => setMaterial('3')}
                    value="3"
                />
                 <Link className="building__type__name" onClick={() => setMaterial('3')}
                       to="/fourth-step">Деревянный брус</Link>
                            </div>
                </div>
            </div>
                :
                <div  className="body__container">
                <p className="container__name">Что будем строить?</p>
                    <div>
                        <div>
                 <input
                     type="radio"
                     checked={material === '2'}
                     onChange={() => setMaterial('2')}
                     value="2"
                 />
                 <Link className="building__type__name" onClick={() => setMaterial('2')}
                       to="/fourth-step">Шлакоблок</Link>
                            </div>
                        <div>
                 <input
                     type="radio"
                     checked={material === '4'}
                     onChange={() => setMaterial('4')}
                     value="4"
                 />
                 <Link className="building__type__name" onClick={() => setMaterial('4')}
                       to="/fourth-step">Металл</Link>
                            </div>
                        <div>
                <input
                    type="radio"
                    checked={material === '5'}
                    onChange={() => setMaterial('5')}
                    value="5"
                />
                 <Link className="building__type__name" onClick={() => setMaterial('5')}
                       to="/fourth-step">Сендвич панели</Link>
                            </div>
                        </div>
            </div>
                    }


            <div  className="button__container">
                <input
                    onClick={() => {
                        clearData()
                        history.push('/')
                    }}
                    type="button"
                    value="Отмена"
                    className="button"
                />
                <input
                    className="button"
                    type="submit"
                    value="Далее"
                    onClick={() => history.push('/fourth-step')}
                />
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    material: state.building.material || '1',
    buildingType: state.building.buildingType
});

const mapDispatchToProps = {
    setMaterial,
    clearData
};

const Container = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ThirdStep);

export default Container;

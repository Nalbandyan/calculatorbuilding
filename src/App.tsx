import React, {FunctionComponent} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import FirstStep from './pages/FirstStep';
import SecondStep from "./pages/SecondStep";
import ThirdStep from "./pages/ThirdStep";
import FourthStep from "./pages/FourthStep";
import Result from "./pages/Result";

const App:FunctionComponent = () =>  {
    return (
      <Router>
        <Route exact path="/" component={ FirstStep }/>
        <Route exact path="/second-step" component={ SecondStep }/>
        <Route exact path="/third-step" component={ ThirdStep }/>
        <Route exact path="/fourth-step" component={ FourthStep }/>
        <Route exact path="/result" component={ Result }/>
      </Router>
    );
}

export default App;

import axios from 'axios';

export const apiUrl = 'https://data.techart.ru/lab/json/';

axios.defaults.baseURL = apiUrl;

class Api {
    static calculateData(data: any) {
        const {building, height, material, sizex, sizey} = data;
        return axios.get(`?building=${Number(building)}&height=${building=== '1'?Number(height):'1'}&material=${Number(material)}&sizex=${Number(sizex)}&sizey=${Number(sizey)}`);
    }
}

export default Api;